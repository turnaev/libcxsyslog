
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <time.h>
#include <string.h>

#include <stdio.h>
#include <errno.h>

#include <syslog.h>

#ifndef CXSYSLOG_H
#define CXSYSLOG_H

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define BUF_MAX 65536

typedef struct log_conf_t {
    char tag[64];
    unsigned int facility_level[LOG_DEBUG + 1];
} log_conf_t;

typedef struct xlog_t {
    int connected;
    int fallback_fd;

    log_conf_t * conf;

    char * conn_addr;
    int conn_addr_len;

    int sock_fd;
    struct sockaddr_un sock;

} xlog_t;


xlog_t * xopenlog(char * addr, int err_stream_fd, log_conf_t * conf);
void xlogclose(xlog_t * log);
int xloglog(xlog_t * log, char * msg, unsigned int facility_priority, char * tag);
int xvlog(xlog_t * log, int level, char * msg, ... );

#endif

