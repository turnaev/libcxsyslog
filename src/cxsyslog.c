
#include "cxsyslog.h"

static void reconnect(xlog_t * log){
    int ret;
    int flags;
    // XXX small bug here: 0 is somewhat valid fd for sock !?
    if(log->sock_fd > 0){
        close(log->sock_fd);
    }

    log->sock_fd = socket(AF_LOCAL, SOCK_DGRAM, 0);
    if(log->sock_fd == -1){
        return;
    }
    if (-1 == (flags = fcntl(log->sock_fd, F_GETFL, 0))){
        flags = 0;
    }
    fcntl(log->sock_fd, F_SETFL, flags | O_NONBLOCK);


    if(log->sock.sun_family){
        memset(&log->sock, 0x00, sizeof(struct sockaddr_un));
    }

    log->sock.sun_family = AF_LOCAL;
    memcpy(log->sock.sun_path, log->conn_addr, log->conn_addr_len);

    ret = connect(log->sock_fd, (const struct sockaddr *)&(log->sock), sizeof(struct sockaddr_un));
    // small bug here for EAGAIN, EISCONN ...
    if(ret != -1){
        log->connected = 1;
    }
}

xlog_t * xopenlog(char * addr, int err_stream_fd, log_conf_t * conf){

    int err_stream_copy;
    int flags;
    xlog_t * log;

    if(err_stream_fd >= 0){
        err_stream_copy = dup(err_stream_fd);
        if(err_stream_copy == -1){
            return NULL;
        }
        if (-1 == (flags = fcntl(err_stream_copy, F_GETFL, 0))){
            flags = 0;
        }
        fcntl(err_stream_copy, F_SETFL, flags | O_NONBLOCK);
    }
    else{
        err_stream_copy = -1;
    }

    log = calloc(1, sizeof(xlog_t));
    if(!log){
        goto err1;
    }

    log->conf = conf;

    log->conn_addr = strdup(addr);
    if(!log->conn_addr){
        goto err2;
    }
    log->conn_addr_len = strlen(addr);
    log->fallback_fd = err_stream_copy;
    reconnect(log);

    return log;
err2:
    free(log);
err1:
    close(err_stream_copy);
    return NULL;
}


void xlogclose(xlog_t * log){
    if(!log){
        return;
    }
    close(log->sock_fd);
    if(log->fallback_fd > 0){
        close(log->fallback_fd);
    }
    free(log->conn_addr);
    free(log);
}

int xvlog(xlog_t * log, int level, char * msg, ...){
    va_list args;
    int ret;
    char buf[BUF_MAX];
    if(log->conf){
        va_start(args, msg);
        ret = vsnprintf(buf, BUF_MAX, msg, args);
        va_end(args);
        if(ret >= BUF_MAX){
            errno = ERANGE;
            return -1;
        }
        return xloglog(log, buf, log->conf->facility_level[level], log->conf->tag);
    }
    errno = EINVAL;
    return -1;
}

int xloglog(xlog_t * log, char * msg, unsigned int facility_priority, char * tag){
    char buf[BUF_MAX];
    int msg_len, ret, sock_fd;
    char * tmp;
    pid_t pid;
    struct timespec ts;

    if(!facility_priority){
        return 0;
    }

    if(!log->connected){
        reconnect(log);
    }

    pid = getpid();

    if(log->connected){
        msg_len = snprintf(buf, BUF_MAX, "<%d>Jan 01 00:00:00 %s[%d]: %s\000", facility_priority, tag, pid, msg );
        if(msg_len >= BUF_MAX){
            errno = ENOBUFS;
            return -1;
        }
        ret = send(log->sock_fd, buf, msg_len, 0);
        if(ret == -1){
            if(errno != EAGAIN && errno != EWOULDBLOCK){
                log->connected = 0;
            }
            return -1;
        }
        return 0;
    }
    else if(log->fallback_fd >= 0){
#ifdef __FreeBSD__
        clockid_t clk = CLOCK_REALTIME_FAST;
#else
        clockid_t clk = CLOCK_REALTIME;
#endif
        if(clock_gettime(clk, &ts) != 0){
            return -1;
        }
        msg_len = snprintf(buf, BUF_MAX, "<%d>%lld.%.9ld %s[%d]: %s\n", facility_priority, (long long)ts.tv_sec, ts.tv_nsec, tag, pid, msg );
        if(msg_len >= BUF_MAX){
            errno = ENOBUFS;
            return -1;
        }
        write(log->fallback_fd, buf, msg_len);
        return 0;
    }

}

/*
#include <syslog.h>

int main(){

    void * log;

    printf("test\n");

    log = xopenlog("/dev/log", STDERR_FILENO);
    if(!log){
        printf("error opening log: %s", strerror(errno) );
    }

    for(int i =0; i < 1000; i++){
        xloglog(log, "Hello world!", LOG_LOCAL0 | LOG_DEBUG, "xfront_gg");
        sleep(1);
    }


    xlogclose(log);
}

*/



